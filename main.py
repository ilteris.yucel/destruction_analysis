from proccess import Process


def main():
    try:
        process = Process()
        process.create_target_list()
        process.calculate_hits()
        process.calculate_destruction_probabilities()
        print(process)
        process.create_screen()
        process.show()
    except Exception as e:
        print(e)


if __name__ == "__main__":
    main()
