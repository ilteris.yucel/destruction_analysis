from abc import ABC, abstractmethod


class AbstractTarget(ABC):
    def __init__(self, target_id: int, material: str, x_low: int, x_high: int, y_low: int, y_high: int, *args, **kwargs):
        self._target_id = target_id
        self._material = material
        self._x_low = x_low
        self._x_high = x_high
        self._y_low = y_low
        self._y_high = y_high
        self._hit_count = 0
        self._destruction_probability = 0.0

    @property
    def target_id(self):
        return self._target_id

    @property
    def material(self):
        return self._material

    @property
    def x_low(self):
        return self._x_low

    @property
    def x_high(self):
        return self._x_high

    @property
    def y_low(self):
        return self._y_low

    @property
    def y_high(self):
        return self._y_high

    @property
    def hit_count(self):
        return self._hit_count

    @property
    def destruction_probability(self):
        return self._destruction_probability

    def hit(self, particle_x: float, particle_y: float) -> bool:
        if self._x_low < particle_x <= self._x_high and self._y_low < particle_y <= self._y_high:
            self._hit_count += 1
            return True
        return False

    @abstractmethod
    def calculate_destruction_probability(self) -> float:
        pass

    def __repr__(self):
        return f"{self.__class__.__name__}({self.target_id, self.material, self.x_low, self.x_high, self.y_low, self.y_high, self.hit_count, self.destruction_probability})"


class Glass(AbstractTarget):

    def calculate_destruction_probability(self) -> float:
        if not self.hit_count:
            self._destruction_probability = 0.0
        else:
            self._destruction_probability = 1.0
        return self._destruction_probability


class Wood(AbstractTarget):
    def calculate_destruction_probability(self) -> float:
        self._destruction_probability = self._hit_count * 0.01
        return self._destruction_probability


class Metal(AbstractTarget):
    def calculate_destruction_probability(self) -> float:
        if self.hit_count <= 50:
            self._destruction_probability = 0.0
        else:
            self._destruction_probability = self._hit_count * 0.02
        return self._destruction_probability

