import tkinter
from target import AbstractTarget
import typing


class Screen(tkinter.Tk):
    def __init__(self):
        super(Screen, self).__init__()
        self.__canvas = tkinter.Canvas(self, bg="white", width=1000, height=200)

    def draw_rectangle(self, target: AbstractTarget) -> None:
        color_dict = {
            "METAL": "red",
            "GLASS": "blue",
            "WOOD": "green"
        }
        self.__canvas.create_rectangle((target.x_low, target.y_low),
                                       (target.x_high, target.y_high),
                                       fill=color_dict[target.material])
        text_x = (target.x_low + target.x_high) / 2
        text_y = (target.y_low + target.y_high) / 2
        self.__canvas.create_text((text_x, text_y),
                                  text=f"Hedef-{target.target_id}\nPk={target.destruction_probability}",
                                  fill="black",
                                  font='tkDefaultFont 8')

    def draw_rectangles(self, target_list: typing.List[AbstractTarget]) -> None:
        for target in target_list:
            self.draw_rectangle(target)

    def show(self) -> None:
        self.__canvas.pack()
        self.mainloop()

