class KuzgunTechException(Exception):
    def __init__(self, namespace: str, function: str, message: str):
        self.namespace = namespace
        self.function = function
        self.message = message
        super(KuzgunTechException, self).__init__(f"Classname: {self.namespace}\nFunction: {self.function}\nMessage: {self.message}")


class DBOperationException(KuzgunTechException):
    def __init__(self, namespace: str, function: str, message: str, connection_settings: dict):
        super(DBOperationException, self).__init__(namespace, function, message)
        print(f"Connection settings: {' '.join([f'{k}:{v}' for k,v in connection_settings.items()])}")


class CustomTypeException(KuzgunTechException):
    def __init__(self, namespace: str, function: str, message: str, attr: str):
        super(CustomTypeException, self).__init__(namespace, function, f"{attr} {message}")


class ProcessException(KuzgunTechException):
    def __init__(self, namespace: str, function: str, message: str):
        super(ProcessException, self).__init__(namespace, function, message)