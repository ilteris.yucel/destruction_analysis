import argparse
import os
import sys
import subprocess as sp
import init_db as db

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
db_settings = {
    "db_name": "kuzguntech",
    "db_user": "postgres",
    "db_pass": "",
    "host": "127.0.0.1",
    "port": 5432
}

path_settings = {
    "particles_file": None,
    "targets_file": None
}

parser = argparse.ArgumentParser(
    prog="setup.py",
    formatter_class=argparse.RawDescriptionHelpFormatter,
    epilog="""
    Setup script to program. It take database connection parameter from argument or
    prompt user. Create .env file to create required environment variable.
    After that install dependency package via pip.
    Run init_db script to create required database schema.
    --particles and --targets arguments are required. --particles argument must be absolute path
    of particles.csv, --targets argument must be absolute path of targets.csv. 
    """
)
parser.add_argument('--dbname', type=str, required=False, help="Specify postgresql db name, default kuzguntech")
parser.add_argument("--dbuser", type=str, required=False, help="Specify postgresql db user, default postgres")
parser.add_argument("--dbpass", type=str, required=False, help="Specify postgresql db user password, default None")
parser.add_argument("--host", type=str, required=False, help="Specify postgresql host, default localhost")
parser.add_argument("--port", type=str, required=False, help="Specify postgresql port, default 5432")
parser.add_argument("--particles", type=str, required=True, help="Specify particles.csv file")
parser.add_argument("--targets", type=str, required=True, help="Specify target.csv file")
parser.print_help()

args = parser.parse_args()

if args.dbname:
    db_settings["db_name"] = args.dbname
elif not args.dbname:
    db_name = input("Specify postgresql db name, default kuzguntech >")
    if db_name:
        db_settings["db_name"] = db_name

if args.dbuser:
    db_settings["db_user"] = args.dbuser
elif not args.dbuser:
    db_user = input("Specify postgresql db user, default postgres >")
    if db_user:
        db_settings["db_user"] = db_user

if args.dbpass:
    db_settings["db_pass"] = args.dbpass
elif not args.dbpass:
    db_pass = input("Specify postgresql db user password, default '' >")
    if db_pass:
        db_settings["db_pass"] = db_pass

if args.host:
    db_settings["host"] = args.host
elif not args.host:
    host = input("Specify postgresql host, default localhost >")
    if host:
        db_settings["host"] = host

if args.port:
    db_settings["port"] = args.port
elif not args.port:
    host = input("Specify postgresql port, default 5432 >")
    if args.port:
        db_settings["port"] = args.port

path_settings["particles_file"] = args.particles
path_settings["targets_file"] = args.targets

with open(".env", "w") as f:
    for k, v in db_settings.items():
        f.write(f"{k.upper()}={v}\n")
    for k, v in path_settings.items():
        f.write(f"{k.upper()}={v}\n")

try:
    cmd = ["pip", "install", "-r", "requirements.txt"]
    sp.check_output(cmd, shell=False)

    db_status_status, connection_settings = db.test_db_connection()
    if not db_status_status:
        print(f"DB connection failed with parameters : {connection_settings}")
        sys.exit(1)
    print(f"DB connection success with parameters : {connection_settings}")

    create_db_status, message = db.create_tables()
    if not create_db_status:
        print(message)
        sys.exit(1)
    print(message)

    create_rows_status, message = db.create_tables_rows()
    if not create_rows_status:
        print(message)
        sys.exit(1)
    print(message)

except Exception as e:
    print(f"Setup operation failed. Reason is {e}")