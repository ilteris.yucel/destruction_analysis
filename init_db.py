import psycopg2
import os
import csv
from dotenv import load_dotenv

load_dotenv()


def test_db_connection() -> (bool, dict):
    try:
        connection_settings_object = {
            "database": os.environ["DB_NAME"],
            "user": os.environ["DB_USER"],
            "password": os.environ["DB_PASS"],
            "host":  os.environ["HOST"] if os.environ["DB_PASS"] else None,
            "port":  os.environ["PORT"] if os.environ["DB_PASS"] else None
        }
        connection = psycopg2.connect(**connection_settings_object)
        cursor = connection.cursor()
        cursor.execute("select version()")
        connection.close()
        return True, connection_settings_object
    except Exception as e:
        print(e)
        return False, connection_settings_object


def create_tables() -> (bool, str):
    try:
        connection_settings_object = {
            "database": os.environ["DB_NAME"],
            "user": os.environ["DB_USER"],
            "password": os.environ["DB_PASS"],
            "host":  os.environ["HOST"] if os.environ["DB_PASS"] else None,
            "port":  os.environ["PORT"] if os.environ["DB_PASS"] else None
        }

        connection = psycopg2.connect(**connection_settings_object)
        cursor = connection.cursor()

        drop_table_targets = '''DROP TABLE IF EXISTS targets'''
        drop_table_particles = '''DROP TABLE IF EXISTS particles'''

        create_table_targets = '''CREATE TABLE targets(
        id INT PRIMARY KEY,
        material VARCHAR(256) NOT NULL,
        xlow INT NOT NULL DEFAULT 0,
        xhigh INT NOT NULL DEFAULT 0,
        ylow INT NOT NULL DEFAULT 0,
        yhigh INT NOT NULL DEFAULT 0,
        pk FLOAT NOT NULL DEFAULT 0.0
        )
        '''

        create_table_particles = '''CREATE TABLE particles(
        id INT PRIMARY KEY,
        x FLOAT NOT NULL DEFAULT 0.0,
        y FLOAT NOT NULL DEFAULT 0.0
        )
        '''

        cursor.execute(drop_table_targets)
        cursor.execute(drop_table_particles)
        cursor.execute(create_table_targets)
        cursor.execute(create_table_particles)

        connection.commit()
        connection.close()
        return True, "targets and particles tables successfully created."
    except Exception as e:
        return False, f"targets and particles tables cannot created for reason: {e}"

def create_tables_rows() -> (bool, str):
    try:
        connection_settings_object = {
            "database": os.environ["DB_NAME"],
            "user": os.environ["DB_USER"],
            "password": os.environ["DB_PASS"],
            "host":  os.environ["HOST"] if os.environ["DB_PASS"] else None,
            "port":  os.environ["PORT"] if os.environ["DB_PASS"] else None
        }

        connection = psycopg2.connect(**connection_settings_object)
        cursor = connection.cursor()

        with open(os.environ["TARGETS_FILE"]) as targets_csv:
            reader = csv.reader(targets_csv, delimiter=",")
            for row in reader:
                if "id" in row:
                    continue
                cursor.execute('''INSERT INTO targets(id, material, xlow, xhigh, ylow, yhigh) VALUES (%s, %s,  %s, %s, %s, %s)''',
                               (int(row[0]), row[1], int(row[2]), int(row[3]), int(row[4]), int(row[5]))
                               )

        with open(os.environ["PARTICLES_FILE"]) as targets_csv:
            reader = csv.reader(targets_csv, delimiter=",")
            for row in reader:
                if "id" in row:
                    continue
                cursor.execute('''INSERT INTO particles(id, x, y) VALUES (%s, %s, %s)''',
                               (int(row[0]), float(row[1]), float(row[2]))
                               )
        connection.commit()
        connection.close()
        return True, "Records are created successfully"
    except Exception as e:
        return False, f"Records cannot created successfully reason: {e}"
