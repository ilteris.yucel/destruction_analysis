import psycopg2
import os
from exceptions import DBOperationException
from dotenv import load_dotenv

load_dotenv()


class DBOperations:

    connection_settings_object = {
        "database": os.environ["DB_NAME"],
        "user": os.environ["DB_USER"],
        "password": os.environ["DB_PASS"],
        "host": os.environ["HOST"] if os.environ["DB_PASS"] else None,
        "port": os.environ["PORT"] if os.environ["DB_PASS"] else None
    }

    @classmethod
    def select_all_from_table(cls, table_name: str) -> list:
        try:

            connection = psycopg2.connect(**cls.connection_settings_object)

            cursor = connection.cursor()
            select_stmt = f'''SELECT * FROM {table_name}'''
            cursor.execute(select_stmt)

            result = cursor.fetchall()
            connection.commit()
            connection.close()

            return result

        except Exception as e:
            raise DBOperationException(
                cls.__class__.__name__,
                cls.select_all_from_table.__name__,
                f"DB operation cannot done successfully reason: {e}",
                cls.connection_settings_object
            )

    @classmethod
    def select_columns_from_table(cls, table_name: str, columns: list) -> list:
        try:
            if not columns or not isinstance(columns, list) or len(columns) < 1:
                columns = ["*"]

            connection = psycopg2.connect(**cls.connection_settings_object)

            cursor = connection.cursor()
            select_stmt = f'''SELECT {', '.join(columns)} FROM {table_name}'''
            cursor.execute(select_stmt)

            result = cursor.fetchall()
            connection.commit()
            connection.close()

            return result

        except Exception as e:
            raise DBOperationException(
                cls.__class__.__name__,
                cls.select_columns_from_table.__name__,
                f"DB operation cannot done successfully reason: {e}",
                cls.connection_settings_object
            )

    @classmethod
    def update_column_by_id(cls, table_name: str,  row_id: int, column_name: str, value):
        # TODO This operation is not effective because just one row update in one transaction.
        # TODO Create more effective function to one column update for all table
        connection = psycopg2.connect(**cls.connection_settings_object)

        cursor = connection.cursor()
        update_stmt = f'''UPDATE {table_name} SET {column_name} = {value} WHERE id={row_id}'''
        cursor.execute(update_stmt)
        connection.commit()
        connection.close()