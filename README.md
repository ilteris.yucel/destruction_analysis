# destruction_analysis

## Description
Analyse destructure probability with python


## Installation
```
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt -y update
sudo apt -y install postgresql-14
sudo apt install python3-tk
```

## Checks

```
sudo systemctl status postgresql
python3 --version
pip --version

```
## Usage
Run `setup.py` to create db and install dependencies
```
python3 setup.py --dbname <YOUR DB NAME> --dbuser <YOUR DB USER> --particles <PARTICLES CSV PATH> --targets <TARGETS CSV PATH>
python3 main.py

```
