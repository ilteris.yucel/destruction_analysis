from target import AbstractTarget, Metal, Wood, Glass
from db_operations import DBOperations
from exceptions import CustomTypeException, ProcessException
import typing
import canvas


class Process:
    def __init__(self):
        self.__target_list = []
        self.__screen = None

    @property
    def target_list(self) -> typing.List[AbstractTarget]:
        return self.__target_list

    @staticmethod
    def create_target(data_tuple: tuple) -> AbstractTarget:
        if data_tuple[1] == "WOOD":
            target = Wood(*data_tuple)
        elif data_tuple[1] == "GLASS":
            target = Glass(*data_tuple)
        elif data_tuple[1] == "METAL":
            target = Metal(*data_tuple)
        else:
            raise CustomTypeException(
                "main",
                "main",
                "is not valid material.",
                data_tuple[1]
            )
        return target

    def create_target_list(self) -> None:
        try:
            for target in DBOperations.select_all_from_table("targets"):
                target_ = self.create_target(target)
                self.__target_list.append(target_)
        except Exception as e:
            raise ProcessException(
                self.__class__.__name__,
                self.create_target_list.__name__,
                str(e)
            )

    def calculate_hits(self) -> None:
        try:
            for particle in DBOperations.select_all_from_table("particles"):
                for target in self.target_list:
                    target.hit(particle[1], particle[2])
        except Exception as e:
            raise ProcessException(
                self.__class__.__name__,
                self.calculate_hits.__name__,
                str(e)
            )

    def calculate_destruction_probabilities(self) -> None:
        try:
            for target in self.target_list:
                target.calculate_destruction_probability()
                DBOperations.update_column_by_id("targets", target.target_id, "pk", target.destruction_probability)
        except Exception as e:
            raise ProcessException(
                self.__class__.__name__,
                self.calculate_destruction_probabilities.__name__,
                str(e)
            )

    def create_screen(self) -> None:
        self.__screen = canvas.Screen()
        self.__screen.draw_rectangles(self.__target_list)

    def show(self) -> None:
        self.__screen.show()

    def __repr__(self) -> str:
        return f"[{', '.join([str(target) for target in self.target_list])}]"
